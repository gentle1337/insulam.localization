# Insulam.Localization #

This repository contains the localisable text for Estranged: Act II, available on Steam: https://store.steampowered.com/app/582890

# Pre-requisites
* Poedit, a graphical .po file editor: https://poedit.net/
* Fork the repository, add your changes in your own fork, and then submit a pull request to merge in your changes.

For help on forking this repository, please see the BitBucket help article: https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html#ForkingaRepository-HowtoForkaRepository